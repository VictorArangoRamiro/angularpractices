'use strict';

angular.module("MyApp").
directive
(
    'showMenuDirective', function()
    {
        return{
            templateUrl : "directives/menu.html",        
            scope: 
            {
                menus: "="
            },
        
            link: function(scope) 
            {
                //
            }
        };
    }
);