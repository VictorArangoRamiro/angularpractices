'use strict';

angular.module("MyApp").
directive('showDataSummonerDirective', function(summonerService)
{
    return{
        templateUrl : "directives/dataSummoner.html",
        scope: 
        {
            data: "="
        },
        
        link: function(scope) 
        {
            //console.log('Summoner ID: ');
            //console.log(scope.data.id);
            
            //summonerService.getSummonerElo();
            // then, catch finally
            scope.information = {};
            scope.show = false;
            
            scope.getQueue = function()
            {
                //console.log('Hola paps xd');
                
                //probar
                summonerService.getSummonerElo(scope.data.id)
                .then
                (
                    function (response)
                    {
                        scope.show = true;
                        scope.information = response.data;
                        //console.log(scope.information);
                    }
                )
                .catch
                (
                    function (response)
                    {
                        scope.show = false;
                        console.log(response);
                    }
                )
                .finally
                (
                    function (response)
                    {
                        console.log('Task finished in submit elo.');
                    }
                );
            }
        }
    };
});