angular.module('MyApp')
.config(function($routeProvider)
{
    $routeProvider
    .when("/",
    {
        templateUrl: "view/pages/main.html"
    })
    .when("/main",
    {
        templateUrl: "view/pages/main.html"
    })
    .when("/show",
    {
        templateUrl: "view/pages/show.html"
    })
    .when("/add",
    {
        templateUrl: "view/pages/add.html"
    })
    .when("/edit",
    {
        templateUrl: "view/pages/edit.html"
    })
    .when("/delete",
    {
        templateUrl: "view/pages/delete.html"
    })
    .when("/examples",
    {
        templateUrl: "view/pages/examples.html"
    })
    .when("/summoner",
    {
        templateUrl: "view/pages/summoner.html"
    });
});