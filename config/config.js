
// Configuraciones para Translate.
/*
var translationsEN =
{
    TITLE: 'Welcome {{name}}',
    SHOW: 'Show users',
    ADD: 'Add user',
    EDIT: 'Edit user',
    DELETE: 'Delete user',
    CHANGE: 'Change language'
};

var translationsES =
{
    TITLE: 'Bienvenido {{name}}',
    SHOW: 'Mostrar usuarios',
    ADD: 'Agregar usuario',
    EDIT: 'Editar usuario',
    DELETE: 'Eliminar usuario',
    CHANGE: 'Cambiar idioma'
};

var myApp = angular.module('MyApp', ['pascalprecht.translate']);

myApp.controller
(
    'firstController',
    function($scope)
    {
        var saludo = "Hello";
        $scope.message = saludo;
    }
)
.config(['$translateProvider', function ($translateProvider) 
{
    
    $translateProvider.useSanitizeValueStrategy(null);
    $translateProvider.translations('en', translationsEN);
    $translateProvider.translations('es', translationsES);
    $translateProvider.preferredLanguage('es');
    
}]);

myApp.controller('Ctrl', function ($scope, $translate) 
{
    $scope.changeLanguage = function () 
    {
        $translate.use('es');
    };
});*/

/*angular.module('MyApp')
.config(function($stateProvider)
    {
        var helloState =
        {
            name: 'hello',
            url: '/hello',
            template: '<h3>hello world!</h3>'
        }
  
        var aboutState =
        {
            name: 'about',
            url: '/about',
            template: '<h3>Its the UI-Router hello world app!</h3>'
        }
  
        $stateProvider.state(helloState);
        $stateProvider.state(aboutState);
    }
);*/

angular.module('MyApp')
/*.config(function($stateProvider)
 {
    var hello =
    {
        name: 'mypage',
        url: '/mypage',
        component: 'mypage'
    }
    
    var myData =
    {
        name: 'mydata',
        url: '/mydata',
        template: '<h3>Welcome to my data!</h3>'
    }
    
    $stateProvider.state(hello);
    $stateProvider.state(myData);
});*/

.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/home');

    $stateProvider
    .state
    (
        'home',
        {
            url: '/home',
            templateUrl: 'view/pages/main.html'
        }
    )
    .state
    (
        'mypage', 
        {
            name: 'mypage',
            url: '/mypage',
            component: 'mypage'
        }
    )
    .state
    (
        'mydata', 
        {
            name: 'mydata',
            url: '/mydata',
            component: 'mydata'
        }
    )
    .state
    (
        'mycontact', 
        {
            name: 'mycontact',
            url: '/mycontact',
            component: 'mycontact'
        }
    )
    .state
    (
        'testing', 
        {
            name: 'testing',
            url: '/testing',
            component: 'testing'
        }
    );

});