'use strict';

angular.module('MyApp')
.component
(
    'componentN',
    {
        templateUrl:  'components/NestedComponents/componentN.html',
           
        controller: function($scope, $rootScope)
        {
            $scope.$on
            (
                "SendDown",
                function (evt, data)
                {
                    $scope.Message = "Component N - Sibling: " + data;
                }
            );
            
            $scope.OnClick = function (evt)
            {
                $rootScope.$broadcast("SendDown", "desde el N");
                //$scope.$emit("SendUp", "desde el 2");
            }
        }
    }
);