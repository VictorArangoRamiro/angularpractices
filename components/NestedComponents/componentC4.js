'use strict';

angular.module('MyApp')
.component
(
    'componentC4',
    {
        templateUrl:  'components/NestedComponents/componentC4.html',
           
        controller: function($scope, $rootScope)
        {
            $scope.$on
            (
                "SendDown",
                function (evt, data)
                {
                    $scope.Message = "Component 4 - Sibling: " + data;
                }
            );
        }
    }
);