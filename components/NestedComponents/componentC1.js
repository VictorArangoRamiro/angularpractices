'use strict';

angular.module('MyApp')
.component
(
    'componentC1',
    {
        templateUrl:  'components/NestedComponents/componentC1.html',
           
        controller: function($scope, $rootScope)
        {
            
            //broadcast the event down
            $scope.OnClick = function (evt)
            {
                $rootScope.$broadcast("SendDown", "desde el 1");
            }
            
            $scope.$on
            (
                "SendDown",
                function (evt, data)
                {
                    $scope.Message = "Component 1 - Down: " + data;
                }
            );

            //handle SendUp event
            $scope.$on
            (
                "SendUp",
                function (evt, data)
                {
                    $scope.Message = "Component 1 - Up: " + data;
                }
            );
        }
    }
);