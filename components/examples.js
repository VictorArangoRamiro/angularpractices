'use strict';

angular.module('MyApp').
component
(
    'summonerList',
    {
        templateUrl: "components/examples.html",
        bindings:
        {
            name1: '@',
            name2: '@',
            array: '=',
            etiqueta: '@',
            func: '&',
            count: '='
        },
        controller: function()
        {
            this.message = "Hello ";
            
            function increment()
            {
                this.count++;
            }
            function decrement()
            {
                this.count--;
            }
            
            this.increment = increment;
            this.decrement = decrement;
        }
    }
);