'use strict';

angular.module('MyApp')
.service('summonerService', function ($http) 
{
    var baseUrlGetSummoner = 'https://la1.api.riotgames.com/lol/summoner/v4/summoners/by-name/';
    var endUrlGetSummoner = '?api_key=';
    var apiKey = 'RGAPI-5db07784-bbf3-4ed6-bbaf-4bab5f124d81'
    var _summoner = '';
    var _summonerID = '';
    var _finalUrl = '';
    
    var baseUrlGetElo = 'https://la1.api.riotgames.com/lol/league/v4/positions/by-summoner/';
    var _finalUrlElo = '';
    
    this.setSummoner = function (summoner)
    {
        _summoner = summoner;
        _finalUrl = baseUrlGetSummoner + _summoner + endUrlGetSummoner + apiKey;
    }

    this.getSummoner = function () 
    {
        return _summoner;
    }

    this.getSummonerData = function ()
    {
        if(_summoner.length == 0)
        {
            alert('Summoner invalid!');
        }
        else
        {
            return $http.get
            (
                _finalUrl
            );
        }
    }
    
    this.getSummonerElo = function(summonerID)
    {
        //console.log('probando');
        //console.log(summonerID);
        _finalUrlElo = baseUrlGetElo + summonerID + endUrlGetSummoner + apiKey;
        return $http.get
        (
            _finalUrlElo
        );
        //console.log('Hola');
    }

});