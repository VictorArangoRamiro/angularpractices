angular.module("MyApp")
.controller("examplesController", function($scope)
{
    $scope.name1 = "Víctor";
    $scope.name2 = "Arango";
    
    $scope.array = 
    [
        {
            name: 'Nexus S',
            snippet: 'Fast just got faster with Nexus S.'
        },
        {
            name: 'Motorola XOOM™ with Wi-Fi',
            snippet: 'The Next, Next Generation tablet.'
        },
        {
            name: 'MOTOROLA XOOM™',
            snippet: 'The Next, Next Generation tablet.'
        }
    ];
    
    
    $scope.etiqueta = 'Label 0102';
    $scope.func = function(label)
    {
        //console.log('Hola');
        $scope.etiqueta = label;
        console.log($scope.etiqueta);
    }
    
    $scope.count = 8;
});