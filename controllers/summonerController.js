'use strict';

angular.module('MyApp')
.controller('summonerController', function ($scope, summonerService, $http)
{
    $scope.data = {};
    $scope.summonerId = '';
    $scope.updateSummoner = function ()
    {
        summonerService.setSummoner($scope.data.summoner);
    };

    $scope.submitSummoner = function ()
    {
        summonerService.getSummonerData()
        .then
        (
            function (response)
            {
                $scope.data = response.data;
                $scope.data.summonerID = response.data['id'];
                $scope.summonerId = response.data['id'];
                $scope.data.summonerName = response.data['name'];
                $scope.data.summonerLevel = response.data['summonerLevel'];
                $scope.data.status = response.status;
            }
        )
        .catch
        (
            function(response) 
            {
                alert('Ha ocurrido un error, código: ' + response.data['status'].status_code);
            }
        )
        .finally
        (
            function()
            {
                console.log("Task finished in submit summoner.");
            }
        );
    }
});